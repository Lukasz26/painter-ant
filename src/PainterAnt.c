#include <stdlib.h>
#include <time.h>
#include <curses.h>
#include "territory.h"
#include "ant.h"

// PDCurses windows
WINDOW* territoryWnd;
WINDOW* monitorWnd;

// Color pair numbers
enum {
    INTERFACE_STYLE = 1,
    TERRITORY_STYLE,
    WALL_STYLE,
    ANT_STYLE,
    PAINT_STYLE,
    MARKER_STYLE
};

// Prepared characters and/or attributes
chtype styles[32];

/**
 * @brief Initializes colors and character styles for window painting
 */
void prepareStyles() {
    init_pair(INTERFACE_STYLE, COLOR_WHITE, COLOR_BLACK);
    styles[INTERFACE_STYLE] = COLOR_PAIR(INTERFACE_STYLE) | A_BOLD;

    init_pair(TERRITORY_STYLE, COLOR_WHITE, COLOR_BLACK);
    styles[TERRITORY_STYLE] = ' ' | COLOR_PAIR(TERRITORY_STYLE) | A_NORMAL;

    init_pair(WALL_STYLE, COLOR_YELLOW, COLOR_RED);
    styles[WALL_STYLE] = ACS_CKBOARD | COLOR_PAIR(WALL_STYLE) | A_NORMAL;

    init_pair(ANT_STYLE, COLOR_WHITE, COLOR_MAGENTA);
    styles[ANT_STYLE] = COLOR_PAIR(ANT_STYLE) | A_BOLD;

    init_pair(PAINT_STYLE, COLOR_BLUE, COLOR_BLUE);
    styles[PAINT_STYLE] = ACS_BLOCK | COLOR_PAIR(PAINT_STYLE) | A_BOLD;

    init_pair(MARKER_STYLE, COLOR_GREEN, COLOR_GREEN);
    styles[PAINT_STYLE] = ACS_BLOCK | COLOR_PAIR(PAINT_STYLE) | A_NORMAL;
}

/**
 * @brief Redraws territory window and current ant position and direction
 */
void paintTerritory() {
    wattrset(territoryWnd, styles[TERRITORY_STYLE]);
    wclear(territoryWnd);
    int y, x;
    for (y = 0; y < TERRITORY_HEIGHT; y++) {
        for (x = 0; x < TERRITORY_WIDTH; x++) {
            switch (territory[y][x]) {
            case WALL:
                mvwaddch(territoryWnd, y, x, styles[WALL_STYLE]);
                break;
            case PAINT:
                mvwaddch(territoryWnd, y, x, styles[PAINT_STYLE]);
                break;
            case MARKER:
                mvwaddch(territoryWnd, y, x, styles[MARKER_STYLE]);
                break;
            default:
                break;
            }
        }
    }
    chtype antChars[] = { '^', '>', 'v', '<', '*' };
    mvwaddch(territoryWnd, theAnt.y, theAnt.x,
             antChars[theAnt.legs->currentState] | styles[ANT_STYLE]);
    wrefresh(territoryWnd);
}

/**
 * @brief Redraws monitor window (for optional future use)
 */
void paintMonitor() {
    // TODO: Display current ant state and other information

    int row = 1;
    mvwaddstr(monitorWnd, row++, 0, "          I - forward");
    mvwaddstr(monitorWnd, row++, 0, "          J - turn left");
    mvwaddstr(monitorWnd, row++, 0, "          L - turn right");
    mvwaddstr(monitorWnd, row++, 0, "          K - turn back");
    row++;
    mvwaddstr(monitorWnd, row++, 0, "         Up - move north");
    mvwaddstr(monitorWnd, row++, 0, "       Down - move south");
    mvwaddstr(monitorWnd, row++, 0, "       Left - move west");
    mvwaddstr(monitorWnd, row++, 0, "      Right - move east");
    row++;
    mvwaddstr(monitorWnd, row++, 0, "Shift+   Up - turn north");
    mvwaddstr(monitorWnd, row++, 0, "Shift+ Down - turn south");
    mvwaddstr(monitorWnd, row++, 0, "Shift+ Left - turn west");
    mvwaddstr(monitorWnd, row++, 0, "Shift+Right - turn east");
    row++;
    mvwaddstr(monitorWnd, row++, 0, "      Space - run/stop ant");
    mvwaddstr(monitorWnd, row++, 0, "        Esc - exit");
    wrefresh(monitorWnd);
}

/**
 * @brief   Handles mouse click or drag events in territory window.
 *          LEFT BUTTON: draw walls
 *          RIGHT BUTTON: erase cells
 * @param x Mouse event column
 * @param y Mouse event row
 */
void processTerritoryMouse(int x, int y) {
    if (BUTTON_CHANGED(1)) {
        short buttonAction = BUTTON_STATUS(1) & BUTTON_ACTION_MASK;
        if (buttonAction != BUTTON_RELEASED) {
            territory[y][x] = WALL;
        }
    }

    if (BUTTON_CHANGED(3)) {
        short buttonAction = BUTTON_STATUS(3) & BUTTON_ACTION_MASK;
        if (buttonAction != BUTTON_RELEASED) {
            if (y != 0 && y != TERRITORY_HEIGHT-1 &&
                x != 0 && x != TERRITORY_WIDTH-1) {
                territory[y][x] = EMPTY;
            }
        }
    }
}

/**
 * @brief      Handles arrow key events.
 *             ARROW:       move ant in arrow direction
 *             SHIFT+ARROW: turn ant in arrow direction
 * @param key  The key pressed
 */
void processArrows(int key) {
    DIRECTION dir = UNKNOWN;
    switch (key) {
        case KEY_UP:    case KEY_SUP:       dir = NORTH; break;
        case KEY_DOWN:  case KEY_SDOWN:     dir = SOUTH; break;
        case KEY_LEFT:  case KEY_SLEFT:     dir = WEST; break;
        case KEY_RIGHT: case KEY_SRIGHT:    dir = EAST; break;
    }
    if (PDC_get_key_modifiers() & PDC_KEY_MODIFIER_SHIFT
        && key != UNKNOWN) {
        theAnt.legs->currentState = dir;
    }
    else {
        translateCell(&theAnt.y, &theAnt.x, dir);
    }
}

/**
 * @brief Performs one step of ant simulation. Sends one input signal to
 *        the ant FSM according to cell type ahead of the ant. If ant brush
 *        is lowered, paints the cell the ant has moved to.
 */
void simulationStep() {
    int y = theAnt.y;
    int x = theAnt.x;
    translateCell(&y, &x, theAnt.legs->currentState);
    switch (territory[y][x]) {
    case WALL:
        processInput(theAnt.brain, WALL_AHEAD); break;
    case PAINT:
        processInput(theAnt.brain, PAINT_AHEAD); break;
    default:
        processInput(theAnt.brain, EMPTY_AHEAD); break;
    }
    if (theAnt.brush->currentState == BRUSH_LOWERED) {
        territory[theAnt.y][theAnt.x] = PAINT;
    }
}

int main() {
    initscr();
    start_color();
    curs_set(0);
    cbreak();
    noecho();
    nodelay(stdscr, TRUE);
    keypad(stdscr, TRUE);
    PDC_save_key_modifiers(TRUE);
    mouse_on(ALL_MOUSE_EVENTS);
    mouseinterval(0);
    refresh();

    monitorWnd = newwin(LINES, 30, 0, 0);
    territoryWnd = newwin(TERRITORY_HEIGHT, TERRITORY_WIDTH, 1, COLS-TERRITORY_WIDTH-1);
    if (!monitorWnd || !territoryWnd) {
        endwin();
        return EXIT_FAILURE;
    }

    prepareStyles();
    initTerritory();
    paintTerritory();

    bool simulationRunning = FALSE;
    clock_t simulationStepInterval = CLOCKS_PER_SEC / 15;
    clock_t lastStepTime = clock();

    bool mainLoop = TRUE;
    while (mainLoop) {
        int key = getch();
        switch (key) {
            case ERR: ;
                clock_t currentTime = clock();
                if (simulationRunning &&
                    currentTime >= lastStepTime + simulationStepInterval) {
                    lastStepTime = currentTime;
                    simulationStep();
                }
                break;

            case KEY_MOUSE:
                request_mouse_pos();
                int x, y;
                wmouse_position(territoryWnd, &y, &x);
                if (x > 0) {
                    processTerritoryMouse(x, y);
                    break;
                }
                break;

            case KEY_UP:    case KEY_SUP:
            case KEY_DOWN:  case KEY_SDOWN:
            case KEY_LEFT:  case KEY_SLEFT:
            case KEY_RIGHT: case KEY_SRIGHT:
                processArrows(key);
                break;

            case 'i': case 'I': processInput(theAnt.legs, FORWARD); break;
            case 'j': case 'J': processInput(theAnt.legs, TURN_LEFT); break;
            case 'l': case 'L': processInput(theAnt.legs, TURN_RIGHT); break;
            case 'k': case 'K': processInput(theAnt.legs, TURN_BACK); break;

            case 040:   // SPACE
                simulationRunning = !simulationRunning;
                break;

            case 033:   // ESCAPE
                mainLoop = FALSE;
                break;
        }
        paintTerritory();
        paintMonitor();
    }

    delwin(territoryWnd);
    delwin(monitorWnd);
    endwin();
    return EXIT_SUCCESS;
}
