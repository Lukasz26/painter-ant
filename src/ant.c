#include "ant.h"
#include "territory.h"

//------------------------------------------------------------------------------
// Helper actions to be used in ant FSM rules
void legForward() { translateCell(&theAnt.y, &theAnt.x, theAnt.legs->currentState); }
void antForward() { processInput(theAnt.legs, FORWARD); }
void antLeft() { processInput(theAnt.legs, TURN_LEFT); }
void antRight() { processInput(theAnt.legs, TURN_RIGHT); }
void antStartPaint() { processInput(theAnt.brush, LOWER_BRUSH); }
void antStopPaint() { processInput(theAnt.brush, RAISE_BRUSH); }

//------------------------------------------------------------------------------
// Leg logic

typedef DIRECTION legStates; // Just a reminder that legs use direction states

RULE legRules[] = {
    { ANY_STATE, NO_SIGNAL, NULL, SAME_STATE },
    // TODO: Add rules managing leg state
};

//------------------------------------------------------------------------------
// Brain logic

enum brainStates {
    IDLE,
    // TODO: Add ant brain states
};

RULE brainRules[] = {
    { ANY_STATE, NO_SIGNAL, NULL, SAME_STATE },
    // TODO: Add rules managing brain state
};

//------------------------------------------------------------------------------
// Brush logic

RULE brushRules[] = {
    { ANY_STATE, NO_SIGNAL, NULL, SAME_STATE },
    // TODO: Add rules managing brush state
};

//------------------------------------------------------------------------------
// The ant construction
// TODO: Check and fix initial FSM states

AUTOMATON legs = { legRules, sizeof(legRules) / sizeof(RULE), 0 };
AUTOMATON brain = { brainRules, sizeof(brainRules) / sizeof(RULE), 0 };
AUTOMATON brush = { brushRules, sizeof(brushRules) / sizeof(RULE), 0 };
ANT theAnt = { 1, 1, &legs, &brain, &brush };

//------------------------------------------------------------------------------

