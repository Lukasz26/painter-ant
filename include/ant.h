#pragma once
#include "fsm.h"

// Ant structure
typedef struct {
    int y;              // Current ant row
    int x;              // Current ant column
    AUTOMATON* legs;    // Ant legs FSM
    AUTOMATON* brain;   // Ant brain FSM
    AUTOMATON* brush;   // Ant brush FSM
} ANT;

// The one global ant instance
extern ANT theAnt;

// Input signals accepted by leg FSM
enum legSignals {
    STAY, FORWARD, TURN_LEFT, TURN_RIGHT, TURN_BACK
};

// Input signals accepted by brain FSM
enum brainSignals {
    WALL_AHEAD, EMPTY_AHEAD, PAINT_AHEAD
};

// Input signals accepted by brush FSM
enum brushSignals {
    RAISE_BRUSH, LOWER_BRUSH
};

// Possible states of brush FSM
enum brushStates {
    BRUSH_RAISED, BRUSH_LOWERED
};
